#include "Dispositivo.h"



Dispositivo::Dispositivo()
{
  //Filas Entradas, Columnas Salidas. 
  pinMode (TECLADO_L1, OUTPUT);
  pinMode (TECLADO_L2, OUTPUT);
  pinMode (TECLADO_L3, OUTPUT);
  pinMode (TECLADO_L4, OUTPUT);
  pinMode (TECLADO_C1, INPUT_PULLUP);
  pinMode (TECLADO_C2, INPUT_PULLUP);
  pinMode (TECLADO_C3, INPUT_PULLUP);
  pinMode (TECLADO_C4, INPUT_PULLUP);

  digitalWrite(TECLADO_L1 ,1);
  digitalWrite(TECLADO_L2 ,1);
  digitalWrite(TECLADO_L3 ,1);
  digitalWrite(TECLADO_L4 ,1);
  
  //CONFIGURACION de SALIDAS RELAY:
  pinMode (RELAY_1, OUTPUT);
  pinMode (RELAY_2, OUTPUT);
  pinMode (RELAY_3, OUTPUT);
  pinMode (RELAY_4, OUTPUT);
  pinMode (RELAY_5, OUTPUT);
  pinMode (RELAY_6, OUTPUT);
  pinMode (RELAY_7, OUTPUT);
  pinMode (RELAY_8, OUTPUT);

  digitalWrite(RELAY_1 ,1);
  digitalWrite(RELAY_2 ,1);
  digitalWrite(RELAY_3 ,1);
  digitalWrite(RELAY_4 ,1);
  digitalWrite(RELAY_5 ,1);
  digitalWrite(RELAY_6 ,1);
  digitalWrite(RELAY_7 ,1);
  digitalWrite(RELAY_8 ,1);
  //buzzer : 
  pinMode (BUZZER, OUTPUT);
  digitalWrite(BUZZER ,0);

}

int Dispositivo::GetKey()
{

bool tecla;
  digitalWrite(TECLADO_L1 ,1);
  digitalWrite(TECLADO_L2 ,1);
  digitalWrite(TECLADO_L3 ,1);
  digitalWrite(TECLADO_L4 ,1); 
  
  
  
  digitalWrite(TECLADO_L1 ,0);
  digitalWrite(TECLADO_L1 ,0);
  
  tecla = digitalRead(TECLADO_C1);
  if(!tecla){return 1;}
  tecla = digitalRead(TECLADO_C2);
  if(!tecla){return 2;}
  tecla = digitalRead(TECLADO_C3);
  if(!tecla){return 3;}
  tecla = digitalRead(TECLADO_C4);
  if(!tecla){return 4;}
  digitalWrite(TECLADO_L1 ,1);
  digitalWrite(TECLADO_L1 ,1);
  digitalWrite(TECLADO_L1 ,1);
//----------------------------------------------
  digitalWrite(TECLADO_L2 ,0);
  digitalWrite(TECLADO_L2 ,0);
  
  tecla = digitalRead(TECLADO_C1);
  if(!tecla){return 5;}
  tecla = digitalRead(TECLADO_C2);
  if(!tecla){return 6;}
  tecla = digitalRead(TECLADO_C3);
  if(!tecla){return 7;}
  tecla = digitalRead(TECLADO_C4);
  if(!tecla){return 8;}
  digitalWrite(TECLADO_L2 ,1);
  digitalWrite(TECLADO_L2 ,1);
  digitalWrite(TECLADO_L2 ,1);
//----------------------------------------------
  digitalWrite(TECLADO_L3 ,0);
  digitalWrite(TECLADO_L3 ,0);
  
  tecla = digitalRead(TECLADO_C1);
  if(!tecla){return 9;}
  tecla = digitalRead(TECLADO_C2);
  if(!tecla){return 10;}
  tecla = digitalRead(TECLADO_C3);
  if(!tecla){return 11;}
  tecla = digitalRead(TECLADO_C4);
  if(!tecla){return 12;}
  digitalWrite(TECLADO_L3 ,1);
  digitalWrite(TECLADO_L3 ,1);
  digitalWrite(TECLADO_L3 ,1);

//----------------------------------------------
  digitalWrite(TECLADO_L4 ,0);
  digitalWrite(TECLADO_L4 ,0);
  
  tecla = digitalRead(TECLADO_C1);
  if(!tecla){return 13;}
  tecla = digitalRead(TECLADO_C2);
  if(!tecla){return 14;}
  tecla = digitalRead(TECLADO_C3);
  if(!tecla){return 15;}
  tecla = digitalRead(TECLADO_C4);
  if(!tecla){return 16;}
  digitalWrite(TECLADO_L4 ,1);
  digitalWrite(TECLADO_L4 ,1);
  digitalWrite(TECLADO_L4 ,1);

  return 0;


}

void Dispositivo::Relays(int n_re, bool valor){
     
     valor = !valor;
     switch(n_re){
       case 1:
               digitalWrite(RELAY_1 ,valor);
               break;
       case 2:
               digitalWrite(RELAY_2 ,valor);
               break;
       case 3:
               digitalWrite(RELAY_3 ,valor);
               break;
       case 4:
               digitalWrite(RELAY_4 ,valor);
               break;
      
       case 5:
               digitalWrite(RELAY_5 ,valor);
               break;
       case 6:
               digitalWrite(RELAY_6 ,valor);
               break;
       case 7:
               digitalWrite(RELAY_7 ,valor);
               break;
       case 8:
               digitalWrite(RELAY_8 ,valor);
               break;
      default:
              digitalWrite(RELAY_1 ,1);
              digitalWrite(RELAY_2 ,1);
              digitalWrite(RELAY_3 ,1);
              digitalWrite(RELAY_4 ,1);
              digitalWrite(RELAY_5 ,1);
              digitalWrite(RELAY_6 ,1);
              digitalWrite(RELAY_7 ,1);
              digitalWrite(RELAY_8 ,1);
              break;
     
     
     }     



}


