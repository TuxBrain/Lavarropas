#ifndef DISPOSOTIVO_H
#define DISPOSITIVO_H


#include <Arduino.h>




/* SHIELD HARDWARE PIN DEFINITIONS */


#define TECLADO_L1 PA4
#define TECLADO_L2 PA3
#define TECLADO_L3 PA2
#define TECLADO_L4 PA1
#define TECLADO_C1 PA0
#define TECLADO_C2 PC15
#define TECLADO_C3 PC14
#define TECLADO_C4 PA5
//CONFIGURACION RELAY:
#define RELAY_1 PB12
#define RELAY_2 PB13
#define RELAY_3 PB14
#define RELAY_4 PB15
#define RELAY_5 PA8
#define RELAY_6 PB5 //PA12
#define RELAY_7 PA11
#define RELAY_8 PA12//PB5

//buzer:
#define BUZZER PB10



class Dispositivo
{
    
public:
             Dispositivo();
        int  GetKey();
        void Relays(int n_re,bool valor);
        
    private:
    
   
};

#endif // 












