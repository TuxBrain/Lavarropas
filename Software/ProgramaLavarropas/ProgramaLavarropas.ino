#include "Dispositivo.h"
#include "uTimerLib.h"
#include <LCD_I2C.h>
#include <HCSR04.h>

LCD_I2C lcd(0x27, 16, 2); // Default address of most PCF8574 modules, change according
#define TRIG  PB8
#define HECHO PB11
UltraSonicDistanceSensor distanceSensor(TRIG, HECHO);  // Initialize sensor that uses digital pins 13 and 12.
#define MAX_LLENO   53//53 :Cuando esta vacio.
#define MIN_VACIO   26//25 :Cuando esta lleno de agua.
#define TIEMPO_GIRO 9


Dispositivo HW;
// PB6 = SCL
// PB7 = SDA

#define LED PC13

enum ACT {MOTOR=1,CENTRIFUGADO,VALVULA_FRIA,BOMBA_AGUA };

volatile bool status = 0,  FlagGiro,Giro_Activado;
int Boton = 0,Agua;
int Datos = 0;
char numero[17];
bool Flag =0,FlagMinuto,FlagLCD;

char ModoTexto[5][14] ={"Ready","MOTOR","CENTRIFUGADO","VALVULA FRIA","BOMBA AGUA"};
int Mensaje = 0,Estado_Relays = 0;


void setup() {
     
     
     Serial.begin(9600);
     TimerLib.setTimeout_us(Tiempos, 25000);
     Serial.print("Version de compilacion : ");
     Serial.println(__TIME__);
     Serial.print("Dia de compilacion : ");
     Serial.println(__DATE__);
     //LCD
     lcd.begin(); 
     lcd.backlight();
     lcd.clear();
     lcd.setCursor(0,0);
     lcd.print(__DATE__); 
     lcd.setCursor(3,1);
     lcd.print(__TIME__); 

     delay(2000);

     pinMode (LED, OUTPUT);
     tone(BUZZER,1200,1200);
     //noTone(BUZZER); 
}

void loop() {
      
      //if(CANTIDAD_DE_VECES >= 0  && Giro_Activado == 1){
         GirarIniciar();
      //}
      
      Datos = Boton;
      if(FlagLCD){
         lcd.clear();
         FlagLCD=0;
         lcd.setCursor(0,1); 
         lcd.print("CM="); 
         Agua = distanceSensor.measureDistanceCm();
         lcd.print(Agua);
         lcd.print("  P :");
         Agua = Porcentaje(Agua);
         lcd.print(Agua);
         lcd.print("%");
         //
         lcd.setCursor(0,0); 
         lcd.print(ModoTexto[Mensaje]);
         if(Estado_Relays)
            lcd.print(" ON");
         else
            lcd.print(" OFF");
      }
     
      if( Porcentaje(Agua) >= 99){

      } 
     

      
      if(Datos != 0){
         lcd.clear();
         Serial.print("BOTON N : ");
         Serial.println(Datos);
         digitalWrite(LED, 0);
         lcd.setCursor(0,0); 
         lcd.print("BOTON N : "); 
         lcd.print(Datos);
         
         if(Datos == 15) 
            Giro_Activado = 1;
         if(Datos == 16){Giro_Activado = 0;HW.Relays(4, 0); }
         noTone(BUZZER);
         tone(BUZZER,1000,100); 
         if(Datos == 1 || Datos == 2 || Datos == 3 || Datos == 4){
            Actuadores(Datos,1);
            Estado_Relays = 1;
            Mensaje = Datos ;
          }
         
         if(Datos == 5 || Datos == 6 || Datos == 7 || Datos == 8){
            Estado_Relays = 0;
            Mensaje = Datos -4;
            Actuadores(Datos-4,0);
         } 
         //HW.Relays(Datos,1);
         Serial.println(Datos);
         Datos=0;
      }
     

}


void  GirarIniciar(void){
static bool girar;
          if(FlagGiro == 1 && Giro_Activado == 1){
             FlagGiro = 0;
             girar = !girar;
             HW.Relays(2, 0);
             HW.Relays(1, 1);
             delay(1200);
             HW.Relays(4,girar);
             delay(1200);
             HW.Relays(2, 1);
             HW.Relays(1, 0);
           }
}


int Porcentaje(int valor ){
int val;  
    val = map(valor, MAX_LLENO,MIN_VACIO,0,100 );
    return val; 
}

void Actuadores(int nombre,bool act ){

    switch (nombre){
      case MOTOR:
                    HW.Relays(2, act);
                    act = !act;
                    HW.Relays(1, act);
                    break;
      case CENTRIFUGADO:   
                                   
                    HW.Relays(5, act);//Activo el embrague de velocidad  
                    delay(2000);
                    HW.Relays(2, act);
                    HW.Relays(4, act);
                    act = !act;
                     
                    HW.Relays(1, act);
                    break;
      case VALVULA_FRIA:
                    HW.Relays(6, act);
                    break;
      case BOMBA_AGUA:
                    HW.Relays(7, act);
                    break;
    
      default:
              HW.Relays(10, 0);//Apaga TODO!!!.
    
    
    }
    


}



int Programas(int opc){




}




void Tiempos() {
	  
 static char contador = 40,contarLCD = 45;
 static int segundos = 60,segGiro = TIEMPO_GIRO ;//,cuenta_lavado = 15;   
    
    TimerLib.setTimeout_us(Tiempos, 25000);
    contador--;
    Boton = HW.GetKey();
    contarLCD--;
    if(!contarLCD){
       FlagLCD=1;
       contarLCD == 50;
    }
    
    if(!contador){
       
       Flag=1;
       status = !status;
	     digitalWrite(LED, status);
       contador = 40; 
       segundos--; 
       segGiro--;
       if(!segGiro){
          segGiro  = TIEMPO_GIRO ;
          FlagGiro = 1;
          //cuenta_lavado--;
          //if(!cuenta_lavado){FlagLavado = 1;}
       }
       
       if(!segundos){
          segundos =60;
          FlagMinuto;
       }
       noTone(BUZZER);
    }

}

/*
void Mensaje(int x,int y,char &texto){
     
    lcd.setCursor(x,y); 
    lcd.print(&texto); 


}
*/